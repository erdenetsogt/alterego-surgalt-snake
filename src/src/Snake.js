import React, { useEffect, useState } from "react";

export default function Snake() {
	const [started, setStarted] = useState(false);
	const [snake, setSnake] = useState([
		{ top: 100, left: 30 },
		{ top: 100, left: 20 },
		{ top: 100, left: 10 },
	]);
	const [direction, setDirection] = useState("right");

	const [food, setFood] = useState({ top: 100, left: 100 });
	const [foodEaten, setFoodEaten] = useState(false);

	const [timer, setTimer] = useState(0);

	function handleKeyDown(event) {
		setDirection((prevDirection) => {
			if (event.key === "ArrowDown") {
				return prevDirection !== "up" ? "down" : prevDirection; // Syntactic sugar
			} else if (event.key === "ArrowRight") {
				return prevDirection !== "left" ? "right" : prevDirection;
			} else if (event.key === "ArrowLeft") {
				return prevDirection !== "right" ? "left" : prevDirection;
			} else if (event.key === "ArrowUp") {
				return prevDirection !== "down" ? "up" : prevDirection;
			}
		});
	}

	useEffect(() => {
		if (started === true) {
			setInterval(incrementTimer, 200);
		}
	}, [started]);

	useEffect(() => {
		if (started === true) {
			move();
		}
	}, [timer]);

	useEffect(() => {
		if (foodEaten) {
			setSnake((prevSnake) => {
				const head = prevSnake[0];
				const newSnake = [...prevSnake, head]; // массив нийлүүлэх үйлдэл
				return newSnake;
			});

			setFoodEaten(false);

			const foodTop = Math.floor(Math.random() * 50) + 1;
			const foodLeft = Math.floor(Math.random() * 50) + 1;
			// TODO могойны биетэй тулгах

			setFood({ top: foodTop * 10, left: foodLeft * 10 });
		}
	}, [foodEaten]);

	useEffect(() => {
		if (started === true) {
			document.addEventListener("keydown", handleKeyDown);
		}
	}, [started]);

	function incrementTimer() {
		setTimer((prevTimer) => {
			return prevTimer + 1;
		});
	}

	function move() {
		let newSnake = [...snake];
		newSnake.pop(); // сүүлийн элэментийг устгадаг
		const head = snake[0];

		// .unshift() -> массивын урд элемэнт нэмдэг

		if (head.top === food.top && head.left === food.left) {
			setFoodEaten(true);
		}

		if (direction === "right") {
			if (head.left === 490) {
				newSnake.unshift({ top: head.top, left: 0 });
			} else {
				newSnake.unshift({ top: head.top, left: head.left + 10 });
			}
		} else if (direction === "left") {
			if (head.left === 0) {
				newSnake.unshift({ top: head.top, left: 490 });
			} else {
				newSnake.unshift({ top: head.top, left: head.left - 10 });
			}
		} else if (direction === "down") {
			if (head.top === 490) {
				newSnake.unshift({ top: 0, left: head.left });
			} else {
				newSnake.unshift({ top: head.top + 10, left: head.left });
			}
		} else if (direction === "up") {
			if (head.top === 0) {
				newSnake.unshift({ top: 490, left: head.left });
			} else {
				newSnake.unshift({ top: head.top - 10, left: head.left });
			}
		}

		const newHead = newSnake[0];

		for (let i = 1; i < newSnake.length; i++) {
			if (newHead.top === newSnake[i].top && newHead.left === newSnake[i].left) {
				setStarted(false);
			}
		}

		setSnake(newSnake);
	}

	return (
		<div>
			<button type="button" onClick={() => setStarted(true)}>
				Эхлэх
			</button>
			<div className="board">
				<div className="food" style={{ top: food.top, left: food.left }}></div>

				{snake.map((item) => (
					<div className="head" style={{ top: item.top, left: item.left }}></div>
				))}
			</div>
		</div>
	);
}
