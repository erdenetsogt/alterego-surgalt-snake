import React, { useEffect, useState } from "react";

export default function App({ name, age }) {
	const [nameShown, setNameShown] = useState(true);
	const [headerShown, setHeaderShown] = useState(true);

	useEffect(() => {
		console.log("App started");

		return () => {
			console.log("App destroyed");
		};
	}, []);

	useEffect(() => {
		console.log("nameShown changed");
	}, [nameShown, name, age]);

	function hideName() {
		setNameShown(false);
	}

	return (
		<div className="header" style={{ textAlign: "center" }}>
			{headerShown && <Header />}
			Hello,
			{nameShown && name},{age}
			{/* DONT DOT THIS onClick={hideName()} */}
			<button onClick={hideName}>Hide name</button>
			<button
				onClick={() => {
					console.log();
					setHeaderShown(false);
				}}
			>
				Hide header
			</button>
		</div>
	);
}

function Header() {
	useEffect(() => {
		console.log("Header started");

		return () => {
			console.log("Header destroyed");
		};
	}, []);

	return <h1>Header</h1>;
}
